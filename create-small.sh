#!/bin/sh
#
# Script to create a EC2 GPU instance for password cracking
# Must have the aws-cli and jq utlities installed
#

AMI="ami-0b023315da407c701"  # Deep Learning AMI (Ubuntu 18.04) Version 33.0
KEY="${KEY:-default value}"
SECURITYGROUP="default"
INSTANCETYPE="p2.xlarge"  # High-performance NVIDIA K80 GPUs, each with 2,496 parallel processing cores and 12GiB of GPU memory
NAMETAG="p2-small"
BUILDSCRIPT="./build.sh"
SSHKEY="${KEY:-~/.ssh/id_rsa}"
SSHUSER="ubuntu"
TMPFILE=`mktemp -t $NAMETAG.XXXX`

echo "Creating $NAMETAG Instance."
aws ec2 run-instances --image-id $AMI \
    --key-name "$KEY" \
    --security-groups "$SECURITYGROUP" \
    --instance-type $INSTANCETYPE \
    --tag-specifications "ResourceType=instance,Tags=[{Key=Name,Value='$NAMETAG'}]" > $TMPFILE
    

INSTANCEID=`cat $TMPFILE | jq -r ".Reservations[] | .Instances[] | .InstanceId"`

if [ -z $INSTANCEID ]; then
    echo ERROR: check $TMPFILE
    exit 255
fi

rm $TMPFILE

STATUS=`aws ec2 describe-instances --instance-ids $INSTANCEID | jq -r .Reservations[0].Instances[0].State.Name`

echo "Instance $INSTANCEID create and in $STATUS status"

while [ "$STATUS" != "running" ]; do
    echo "Waiting for system to boot..."
    sleep 30
    STATUS=`aws ec2 describe-instances --instance-ids $INSTANCEID | jq -r .Reservations[0].Instances[0].State.Name`
    echo "Status: $STATUS"
done

AWSVM_IP=`aws ec2 describe-instances --instance-ids $INSTANCEID | jq .Reservations[0].Instances[0].PublicIpAddress |egrep -o "([0-9]{1,3}\.){3}[0-9]{1,3}"`

echo "Back off"
sleep 15

echo "Copying Build script"
scp -i $SSHKEY $BUILDSCRIPT $SSHUSER@$AWSVM_IP:

echo "SSH to the IP $AWSVM_IP"