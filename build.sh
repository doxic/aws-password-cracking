#!/bin/sh
#
# Configure vanilla Ubuntu Deployment for Hashcat
# Tested on AWS Ubunuti Deep Learning 17.0
#

check_dpkg () {
    if [ -e /var/lib/dpkg/lock ]; then
        printf "Waiting for other instances of apt to complete"
        while sudo fuser /var/lib/dpkg/lock >/dev/null 2>&1; do
            printf "."
            sleep 5
        done
        echo ". done"
    fi
}

check_dpkg
sudo apt update
dpkg -l | grep -qw p7zip-full || sudo apt-get install --yes p7zip-full
dpkg -l | grep -qw crunch || sudo apt-get install --yes crunch

if [ ! -e /usr/bin/hashcat ]; then
    echo "Downloading hashcat"
    wget --directory-prefix=/home/ubuntu --no-clobber https://hashcat.net/files/hashcat-6.1.1.7z 
    7z x -o/home/ubuntu /home/ubuntu/hashcat-6.1.1.7z
    sudo ln -s /home/ubuntu/hashcat-6.1.1/hashcat.bin /usr/bin/hashcat
fi

if [ ! -e /home/ubuntu/hashcat-6.1.1/rockyou.txt ]; then
    echo "Downloading rockyou"
    wget -nH --directory-prefix=/home/ubuntu --no-clobber http://downloads.skullsecurity.org/passwords/rockyou.txt.bz2
    bunzip2 -c /home/ubuntu/rockyou.txt.bz2 > /home/ubuntu/hashcat-6.1.1/rockyou.txt
fi

chown -R ubuntu /home/ubuntu/hashcat-6.1.1

if [ ! -e ~/.updated ]; then 
    check_dpkg
    sudo apt-get --yes upgrade
    touch ~/.updated
    sudo reboot
    exit 1
fi 