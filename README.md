# AWS Password Cracking
> Environment for password cracking demo

Creating a spot instance with Terraform and install hashcat. Step-by-step hands-on commands to demonstrate GPU-assisted password cracking.

## Prerequisites
Clone this repository locally
```shell
$ git clone https://gitlab.com/doxic/aws-password-cracking.git
```

Install Terraform
```sh
$ curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
$ sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
$ sudo apt-get update && sudo apt-get install terraform
```

Optional, install `direnv` and `git-crypt` for environment variables and encryption in git repository.
```sh
$ sudo apt update && sudo apt install --yes git-crypt direnv
```

Alternative: Create `.env` file
```sh
$ cat << EOF > .env
#!/bin/bash
export TF_VAR_AWS_ACCESS_KEY_ID=<AWS_ACCESS_KEY_ID>
export TF_VAR_AWS_SECRET_ACCESS_KEY=<AWS_SECRET_ACCESS_KEY>

export TF_VAR_AWS_DEFAULT_REGION=eu-west-1

export TF_VAR_CF_EMAIL=<CLOUDFLARE_EMAIL>
export TF_VAR_CF_API_KEY=<CLOUDFLARE_API_KEY>
export TF_VAR_CF_ZONE_ID=<CLOUDFLARE_ZONE_ID>
EOF
```

## Usage
In production, you should [store your state remotely](https://learn.hashicorp.com/tutorials/terraform/cloud-migrate). For testing purposes, you can omit the backend from the configuration.
```sh
backend "s3" {
    ...
}
```

Create a new SSH key pair. In production, this key should be protected by a passphrase.
```sh
ssh-keygen -q -N "" -f ~/.ssh/id_rsa_spot
```

Initialize the directory with `terraform init`
```sh
$ terraform init
```

Create infrastructure
```sh
$ terraform apply
[...]
Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes
[...]
Apply complete! Resources: 4 added, 0 changed, 0 destroyed.
```

In case of `Max spot instance count exceeded`, set the variable `create_spot_instance` to `false`
```sh
terraform apply -var="create_spot_instance=false"
```

Connect to your instance over `ssh`
```sh
$ ssh -i ~/.ssh/id_rsa_spot ubuntu@spot.<your domain>
```

## Slides
Generate png from Google Slide pdf. Requires `ImageMagick`
```sh
$ convert SecurePasswords.pdf slide_%02d.png
$ find . -type f -name "*.png" -exec echo '![]({})' ";"
```

## Sources
* [How to pass the templatefile function to user_data argument of EC2 resource in Terraform 0.12? - Stack Overflow](https://stackoverflow.com/questions/62598410/how-to-pass-the-templatefile-function-to-user-data-argument-of-ec2-resource-in-t)
* [Create an EC2 Spot Instance with Terraform - Thoughts by Thomas Derflinger](https://www.tderflinger.com/en/ec2-spot-with-terraform)
* [Password Cracking - Computerphile - YouTube](https://www.youtube.com/watch?v=7U-RbOKanYs)
