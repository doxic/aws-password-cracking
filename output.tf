output "spot_bid_status" {
  value       = aws_spot_instance_request.this.*.spot_bid_status
  description = "The current bid status of the Spot Instance Request"
}