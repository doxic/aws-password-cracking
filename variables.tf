variable "CF_EMAIL" {}
variable "CF_API_KEY" {}
variable "CF_ZONE_ID" {}

variable "create_spot_instance" {
  default = true
}