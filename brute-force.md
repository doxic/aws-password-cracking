# Brute Force Passwords
## Script
```sh
less example0.hash
```

md5: 128bit hashes, never use it
```sh
echo -n Hallo | md5sum
echo -n Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. | md5sum
```



```sh
wc -l  example0.hash
```

From password1234 up to very complex

sha1/md5. lazy developers. Legacy

hashing that takes longer

### Brute Force

Brute-force mode: aaa, aab, aac, ..., aba, ..., ZZZ
```sh
crunch 3 3
curl -O https://raw.githubusercontent.com/jaalto/external-sf--crunch-wordlist/master/charset.lst
crunch 6 6 -f charset.lst mixalpha-numeric-all-space -o bruteforce
```

Some devs only allow lower case letters, maybe

**7 letters**

```
hashcat -O -a 3 example0.hash ?l?l?l?l?l?l?l
```

2 Sec

**8 letters**

```
hashcat -O -a 3 example0.hash ?l?l?l?l?l?l?l?l
```

longer

**6 letters, 2 digits**

many passwords require two digits at the end

```sh
crunch 3 3 abcdefghijklmnopqrstuvwxyz0123456789
```

```
hashcat -O -a 3 example0.hash ?l?l?l?l?l?l?d?d
```

7 secs



Naive Bruteforce

26 lower case, power of 7

with symbolys and lower/upper case ~ 90 cases ^ 7

Masks are cumbersome, with 8-10 its becoming impractical, even with md5

![img](https://www.betterbuys.com/estimating-password-cracking-times/assets/images/password_time_and_length.jpg)

### Dictionary attacks

**wordlist**

Commonly used passwords

Englisch, Deutsch, Manga-Dict, usw

```
hashcat -O -a 0 example0.hash example.dict
```

128416 words, only one found

**wordlist + rules**

common substitiucions, leet speak, toggle case (first letter uppercase)

```
hashcat -O -a 0 example0.hash example.dict -r rules/dive.rule
```

12'724'227'776, found 1759

swearing, two words, fill with year



### Level Up Dictionary attacks

rockyou, game changer. 14 million passwords, proper passwords from a gaming service

rockyou list, big impact, real passwords

breach compilation has ~1.4 billion (hundreds of millions)

```
hashcat -O -a 0 example0.hash ../wordlists/rockyou.txt -r rules/dive.rule
```

https://i.imgflip.com/2opzcs.jpg

1'421'327'633'024





## Amazon EC2 pricing

**g3s.xlarge**

MD5 

OpenCL 11440.2 MH/s

CUDA-docker 11849.6 MH/s

CUDA-native 12246.7 MH/s

49541.6 MH/s



SHA2-256

OpenCL 1419.5 MH/s

CUDA-docker 1462.6 MH/s

CUDA-native 1460.2 MH/s

5967.4 MH/s



**p2.xlarge**

MD5

CUDA-native 4485.7 MH/s



SHA2-256

CUDA-native 821.1 MH/s

## Amazon Machine Image (AMI)

**Deep Learning AMI (Ubuntu 18.04) Version 33.0** - ami-0b023315da407c701





## 

[Amazon EC2 Instance Types Accelerated Computing](https://aws.amazon.com/ec2/instance-types/#Accelerated_Computing)

[Accelerating Password Recovery: GPU Acceleration, Distributed and Cloud Attacks](https://blog.elcomsoft.com/2020/04/accelerating-password-recovery-gpu-acceleration-distributed-and-cloud-attacks/)

[docker-hashcat](https://github.com/javydekoning/docker-hashcat)

[Hashcat in AWS EC2]( https://ben.the-collective.net/2019/07/10/hashcat-in-aws-ec2/)





## Setup

Login with 

```
ssh -i ~/.ssh/aws-doxic.id_rsa ubuntu@ec2-52-212-78-219.eu-west-1.compute.amazonaws.com
```

Update and reboot

```
sudo apt update && sudo apt -y upgrade
sudo reboot
```

To verify that the drivers are working you can run `sudo nvidia-smi`

Install hashcat ([check version](https://hashcat.net/hashcat/))

```
wget https://hashcat.net/files/hashcat-6.1.1.7z
7z x hashcat-*
sudo ln -s $(pwd)/hashcat-6.1.1/hashcat.bin /usr/bin/hashcat
```



### Wordlists

Skullsecurity

```
# a high quality compact wordlist
wget http://downloads.skullsecurity.org/passwords/phpbb.txt.bz2
bzip2 -d phpbb.txt.bz2
```

SecLists (complete) + rockyou

```
mkdir ~/wordlists
git clone https://github.com/danielmiessler/SecLists.git ~/wordlists/seclists
wget -nH http://downloads.skullsecurity.org/passwords/rockyou.txt.bz2 -O ~/wordlists/rockyou.txt.bz2
cd ~/wordlists
bunzip2 ./rockyou.txt.bz2
cd ~
```

SecLists (small)

```
git clone --depth 1 https://github.com/danielmiessler/SecLists.git
```

Rockyou 

```
wget -nH http://downloads.skullsecurity.org/passwords/rockyou.txt.bz2
bunzip2 ./rockyou.txt.bz2
```

Linkedin Hashes

```
wget https://github.com/op7ic/linkedin_wordlist/raw/master/linkedin_opt7ic_pot.tar.bz2
tar -xf ./linkedin_opt7ic_pot.tar.bz2
```



### Running hashcat

Enable optimized kernels (limits password length) `-O `

Enable a specific workload profile "High" `-w 3`

```
nvidia-docker run javydekoning/hashcat:latest hashcat --username -m 1800 ./megashadow256.txt wordlists/rockyou.txt -r hashcat-5.1.0/rules/best64.rule -O -w 3
```

* Append -O to the commandline.
  This lowers the maximum supported password- and salt-length (typically down to 32).

* Append -w 3 to the commandline.
  This can cause your screen to lag.

* Update your backend API runtime / driver the right way:
  https://hashcat.net/faq/wrongdriver