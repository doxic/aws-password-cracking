terraform {
  backend "s3" {
    bucket         = "doxic-remote-backend-state"
    dynamodb_table = "doxic-remote-backend-locks"

    key = "aws-password-cracking/terraform.tfstate"

    encrypt = true
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.26.0"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 2.17.0"
    }
  }
}

provider "aws" {}

# Configure the Cloudflare provider
provider "cloudflare" {
  email   = var.CF_EMAIL
  api_key = var.CF_API_KEY
}

# Provision spot instance with Deep Learning AMI
resource "aws_spot_instance_request" "this" {
  count                  = var.create_spot_instance ? 1 : 0
  ami                    = "ami-09ba01d9b2eb740ba"
  instance_type          = "g3s.xlarge"
  spot_type              = "one-time"
  block_duration_minutes = "120"
  wait_for_fulfillment   = true

  key_name = aws_key_pair.this.key_name

  vpc_security_group_ids = [
    aws_security_group.this.id
  ]

  user_data = file("build.sh")

  tags = {
    Name = "CheapWorker"
  }
}

resource "aws_instance" "this" {
  count         = var.create_spot_instance ? 0 : 1
  ami           = "ami-09ba01d9b2eb740ba"
  instance_type = "g3s.xlarge"

  key_name = aws_key_pair.this.key_name

  vpc_security_group_ids = [
    aws_security_group.this.id
  ]

  user_data = file("build.sh")

  tags = {
    Name = "CheapWorker"
  }
}

resource "aws_key_pair" "this" {
  key_name   = "spot_key"
  public_key = file("~/.ssh/id_rsa_spot.pub")
}

# Create Cloudflare DNS record
resource "cloudflare_record" "spot" {
  count   = var.create_spot_instance ? 1 : 0
  zone_id = var.CF_ZONE_ID
  name    = "spot"
  value   = aws_spot_instance_request.this[0].public_ip
  type    = "A"
  proxied = false
}

resource "cloudflare_record" "instance" {
  count   = var.create_spot_instance ? 0 : 1
  zone_id = var.CF_ZONE_ID
  name    = "spot"
  value   = aws_instance.this[0].public_ip
  type    = "A"
  proxied = false
}

resource "aws_security_group" "this" {
  name        = "ssh-security-group"
  description = "Allow SSH traffic"

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "terraform"
  }
}